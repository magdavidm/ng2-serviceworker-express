//'use strict';

var System = require('systemjs');

//import System from 'systemjs';

//Modules
var express = require('express');
var path = require('path');

var app = express();
var rootPath = path.normalize(__dirname);
// var clientPath = rootPath + '/client';
var nodePort: number = 2133;

var clientPath = String(path.resolve(__dirname, '../client'));

app.use('/node_modules', express.static(path.resolve(__dirname, '../node_modules'))); //this exposes node modules to client, DO NOT do in production

app.use(express.static(path.resolve(__dirname, '../client')));

// var appPath = path.normalize(path.resolve(__dirname, '../')) + "/client/app"; 
// app.use('/app', express.static(appPath));
// app.use('/app', express.static(path.resolve(__dirname, '../client')));


app.get('/', function (req, res) {
    console.log(clientPath);
    res.sendFile(path.resolve(__dirname, '../client/index.html'));
});
// app.get('/dashboard', function (req, res) {
//     // console.log(clientPath);
//     console.log(appPath);
//     // res.send("Server dashboard Response");
//     // res.sendFile(rootPath + '/index.html');
//     res.sendFile(appPath + '/main.ts');
// });

app.listen(nodePort);
console.log(' Listening on port: ' + nodePort);