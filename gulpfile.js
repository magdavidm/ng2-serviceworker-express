var path = require('path');
var gulp = require('gulp');
var ts = require('gulp-typescript');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
var exec = require('child_process').exec;

gulp.task('buildServer', function () {
	var tsProject = ts.createProject(path.resolve('./server/tsconfig.json'));
	return gulp.src(path.resolve('./server/**/*.ts'))
		.pipe(ts(tsProject))
		.js
		.pipe(gulp.dest(path.resolve('./server')))
});

gulp.task('buildClient', function () {
	var tsProject = ts.createProject(path.resolve('./client/tsconfig.json'));
	return gulp.src(path.resolve('./client/**/*.ts'))
		.pipe(ts(tsProject))
		.js
		.pipe(gulp.dest(path.resolve('./client')))
});

gulp.task('buildServe', ['buildClient', 'buildServer'], callback => {
    exec('node server/server.js', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        // cb(err);
    })
});

gulp.task('copyLib', callback => {
	gulp.src([
		// 'node_modules/jquery/dist/jquery.min.js',
		// 'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'node_modules/es6-shim/es6-shim.min.js',
		'node_modules/es6-promise/dist/es6-promise.min.js',
		'node_modules/zone.js/dist/zone.js',
		'node_modules/reflect-metadata/Reflect.js',
		// 'node_modules/systemjs/dist/system-polyfills.js',
		'node_modules/systemjs/dist/system.src.js',
		// 'system.config.js',
	])
		.pipe(concat('vendors.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('client/app'));
});

gulp.task('build', function () {
    gulp.run('buildServer');
    gulp.run('buildClient');
});

gulp.task('default', function () {
    // gulp.run('buildServer');
    gulp.run('buildServe');
});