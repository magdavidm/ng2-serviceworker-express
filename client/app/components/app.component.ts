import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `
  <h1>NG2 App</h1>
  <div>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean nibh neque, porttitor sit amet nulla eget, vulputate feugiat ipsum. Suspendisse id convallis nunc. Donec fermentum eget lectus a dignissim. Integer quis venenatis ipsum, congue pulvinar leo. Aliquam dignissim tincidunt tortor, a fringilla augue. Aenean viverra laoreet orci et interdum. Nulla in eros ullamcorper, hendrerit urna in, iaculis metus. Sed porta diam eu hendrerit ultrices. Duis faucibus mi sed felis congue, a luctus ligula cursus. Suspendisse sollicitudin neque neque, iaculis finibus sapien egestas quis. Sed et cursus ligula.

Curabitur pretium a ex id porttitor. Mauris posuere orci ac molestie ultricies. Nam aliquet nunc sem, ac fermentum lacus consequat efficitur. Vestibulum in mauris nec nisl ultricies sagittis facilisis vitae dui. Nam libero massa, tincidunt ut velit at, pharetra eleifend mi. In id ipsum nisl. Vestibulum pellentesque massa velit, eu interdum mi congue non. Nam sed tellus vestibulum, accumsan magna eget, pharetra metus. Sed arcu orci, scelerisque quis molestie ac, semper vulputate tortor. Cras sollicitudin nisi non eros euismod euismod. Etiam porttitor diam quam, at consequat metus lobortis eu. Integer maximus turpis sem, at varius augue iaculis eget. Donec hendrerit cursus arcu, ut tempor mi rutrum a.

Suspendisse ipsum turpis, placerat id sem at, mollis imperdiet metus. Fusce ut sem porta, posuere turpis at, laoreet nibh. Maecenas lacinia nulla quis purus ultrices sodales. Vivamus odio ipsum, aliquam ut nisi sed, mattis maximus tortor. In ex turpis, tempus et vestibulum eu, iaculis eu ipsum. Donec gravida purus nunc, vel ultricies est interdum euismod. Suspendisse blandit ultricies tortor, in fermentum risus sagittis vitae. Praesent vestibulum sollicitudin ipsum, sit amet posuere lectus finibus vel.
  <div>
  `
})
export class AppComponent { }