var caches: any;
var fetch: any;
interface Window {
  clients: any,
  fetch: any,
  caches: any,
  skipWaiting: any
}

this.addEventListener('install', e => {
  e.waitUntil(
    caches.open("ngswbasic").then(cache => {
      return cache.addAll([
        '/register-service-worker.js',
        '/',
        '/index.html',
        '/index.html?homescreen=1',
        '/?homescreen=1',
        '/styles.css',
        '/app/gears-144x144.png',
        '/app/vendors.min.js',
        '/app/main.js',
        '/systemjs.config.js',
        '/app/components/app.component.js',
        '/node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
        '/node_modules/@angular/core/bundles/core.umd.js',
      ])
        .then(() => self.skipWaiting());
    })
  )
});

this.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

this.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request);
    })
  );
});